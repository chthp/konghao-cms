package org.konghao.basic.model;

import java.util.List;

/**
 * 类Pager.java的实现描述：分页
 * 
 * @author wb-dumao 2014年12月25日 上午10:51:32
 */
public class Pager<T> {

    /**
     * 分页的大小
     */
    private int     size;
    /**
     * 分页的起始页
     */
    private int     offset;
    /**
     * 总记录数
     */
    private long    total;
    /**
     * 分页的数据
     */
    private List<T> datas;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

}
